package com.pharmacy.app.libs;

import android.util.Log;

public class APIs {

    public static final boolean STATUS_SUCCESS = true;
    public static final boolean STATUS_FAIL = false;

    public static final String TAG_SaveApplicationForm = "SaveApplicationForm";

    public static final String base_url = "http://mediabanqpakistan.com";
    public static final String url_path = "/apis/apis.php?";
    public static final String url_path_uploads = "/apis/uploads";

    public static String[] getRegisterApi(String name, String email, String phone, String password) {

        String url = base_url + url_path + "type=add&para=mem&name=" + name + "&email=" + email + "&phone=" + phone + "&password=" + password;
        String tag = "RegisterApi";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getLoginApi(String email, String password, boolean isAdminLogin) {
        String url = "";
        if (isAdminLogin) {
            url = base_url + url_path + "para=login_member&type=1&email=" + email + "&password=" + password + "";
        } else {
            url = base_url + url_path + "para=login_member&email=" + email + "&password=" + password + "";
        }

        String tag = "LoginApi";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getPharmaciesAPIUrl() {

        String url = base_url + url_path + "para=pharmacies&limit=5000";

        String tag = "getPharmaciesAPIUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getPharmacyBranchesAPIUrl(String pid) {

        String url = base_url + url_path + "para=branches&limit=5000&pid=" + pid + "";

        String tag = "getPharmacyBranches";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getMedicinesAPIUrl(String bid) {

        // http://mediabanqpakistan.com/apis/apis.php?para=medicines&limit=5000&bid=11
        String url = base_url + url_path + "para=medicines&limit=5000&bid=" + bid + "";

        String tag = "getMedicinesAPIUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }


    public static String getImageUrl(String imgFile) {

        String url = base_url  + url_path_uploads + "/" + imgFile;
        Log.e("getImageUrl", url);
        return url;
    }

    public static String[] getUploadImageUrl() {

        String url = base_url + url_path + "para=img";
        String tag = "UploadImage";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getAddPharmacyUrl(String name, String phone, String email, String desc) {

        // http://mediabanqpakistan.com/apis/apis.php?para=ph&name=api_name&desc=desc_api&phone=03022222&email=api_email&type=add
        String url = base_url + url_path + "para=ph&type=add&name=" + name + "&phone=" + phone + "&email=" + email + "&desc=" + desc + "";
        String tag = "getAddPharmacyUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getAddPharmacyBranchUrl(String pid, String name, String phone, String email, String desc) {

        // http://mediabanqpakistan.com/apis/apis.php?para=ph&name=api_name&desc=desc_api&phone=03022222&email=api_email&pid=-1&type=add
        String url = base_url + url_path + "para=br&type=add&name=" + name + "&phone=" + phone + "&email=" + email + "&desc=" + desc + "&pid=" + pid;
        String tag = "getAddPharmacyBranchUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getUpdatePharmacyUrl(String id, String name, String phone, String email, String desc) {

        // http://mediabanqpakistan.com/apis/apis.php?para=ph&name=api_name&desc=desc_api&phone=03022222&email=api_email&type=add
        String url = base_url + url_path + "para=ph&type=up&id=" + id + "&name=" + name + "&phone=" + phone + "&email=" + email + "&desc=" + desc + "";
        String tag = "getUpdatePharmacyUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }

    public static String[] getAddMedicineUrl(String pid, String bid, String name, String price, String date_manuf, String date_expiry, String desc, String imagePath) {

        // http://mediabanqpakistan.com/apis/apis.php?para=md&name=api_name&desc=desc_api&price=22222&date=12-11-2019&bid=1&type=add
        String url = base_url + url_path + "para=md&type=add&pid=" + pid + "&bid=" + bid + "&name=" + name + "&price=" + price + "&mdate=" + date_manuf + "&date=" + date_expiry + "&desc=" + desc + "&img=" + imagePath + "";
        String tag = "getAddMedicineUrl";
        Log.e(tag, url);
        String[] url_array = {tag, url};

        return url_array;
    }
}