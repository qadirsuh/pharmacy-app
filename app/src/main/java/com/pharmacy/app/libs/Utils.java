package com.pharmacy.app.libs;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;

import com.google.gson.Gson;
import com.pharmacy.app.BaseApplication;
import com.pharmacy.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Utils {


    public static JSONArray sortJsonArray(JSONArray array, final String compareWith) {

        List<JSONObject> jsons = new ArrayList<JSONObject>();
        for (int i = 0; i < array.length(); i++) {
            try {
                jsons.add(array.getJSONObject(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.sort(jsons, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject lhs, JSONObject rhs) {
                Integer lid = 0;
                try {
                    lid = lhs.getInt(compareWith);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Integer rid = 0;
                try {
                    rid = rhs.getInt(compareWith);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                // Here you could parse string id to integer and then compare.
                return lid.compareTo(rid);
            }
        });
        return new JSONArray(jsons);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static void setError(EditText editText, String error_msg) {
        editText.setError(error_msg);
        editText.requestFocus();
    }

    public static JSONObject convertToJsonObject(Object mObject) {

        try {

            Gson gson = new Gson();
            String json_str = gson.toJson(mObject);
            JSONObject jsonObject = new JSONObject(json_str);

            return jsonObject;

        } catch (Exception e) {

            e.printStackTrace();
//            Utils.showFailInfoAlert("Execption", e.getMessage(), "OK" , "OK" , null, null);
        }

        return null;
    }


    public static ProgressDialog progressDialog(String title) {

        try {

            ProgressDialog dialog = new ProgressDialog(BaseApplication.getCurrentActivity()); // this = YourActivity
            dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            dialog.setMessage(title);
            dialog.setIndeterminate(true);
            dialog.setCanceledOnTouchOutside(false);
            //dialog.show();

            return dialog;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showFailInfoAlert(String title, String message, String button_yes, final Runnable runnablePositive) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseApplication.getCurrentActivity());
        alertDialog.setTitle(title);

        try {
            if (message.contains("Failed to load")) {
                alertDialog.setCancelable(false);
            }
            alertDialog.setMessage(message);
        } catch (Exception e) {
            e.printStackTrace();
        }

        alertDialog.setIcon(R.drawable.ic_alert_fail);
        alertDialog.setPositiveButton(button_yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (runnablePositive != null) {
                    runnablePositive.run();
                }
            }
        });

        alertDialog.show();
    }

    public static void showSuccessAlert(String title, String message, String button_text, final Runnable runnable) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BaseApplication.getCurrentActivity());
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon(R.drawable.ic_alert_success);
        alertDialog.setPositiveButton(button_text, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (runnable != null) {
                    runnable.run();
                }
            }
        });

        alertDialog.show();
    }

    public static final String PREFS_NAME = "PHARAMACY_USER_LOGIN";
    private static SharedPreferences prefs = BaseApplication.getAppContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

    public static void saveSharedPrefRef(String key, String value) {

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void saveSharedPrefRef(String key, Boolean value) {

        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String getSharedPrefRef(String key) {

        try {
            String pref = prefs.getString(key, null);
            Log.e("get pref", pref + "|abc");
            return pref;
        } catch (ExceptionInInitializerError e) {
            Log.e("ExceptionIn", e.getMessage());
            Log.e("ExceptionInCase", e.getCause().getMessage());
            e.printStackTrace();
        }


        return null;
    }

    public static boolean getSharedPrefRefboolean(String key) {

        try {
            boolean pref = prefs.getBoolean(key, false);
            Log.e("get pref", pref + "|abc");
            return pref;
        } catch (ExceptionInInitializerError e) {
            Log.e("ExceptionIn", e.getMessage());
            Log.e("ExceptionInCase", e.getCause().getMessage());
            e.printStackTrace();
        }


        return false;
    }

    public static void clearAllLoginSharePref() {

        SharedPreferences.Editor editor = prefs.edit();

        editor.remove("id");
        editor.remove("name");
        editor.remove("email");
        editor.remove("phone");
        editor.remove("status");
        editor.remove("isAdminLogin");

        editor.apply();
        editor.commit();
    }
}

