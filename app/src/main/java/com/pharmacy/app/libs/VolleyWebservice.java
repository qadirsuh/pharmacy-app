package com.pharmacy.app.libs;

import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.pharmacy.app.BaseApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class VolleyWebservice {

    private VolleyResponse mVolleyResponse;

    public void setVolleyLister(VolleyResponse mVolleyResponse) {
        this.mVolleyResponse = mVolleyResponse;
    }

    public void makeJsonObjectRequest(final String requestTag, final String url, int method, JSONObject jsonObject) {

        Log.e("Params", jsonObject.toString());

        final ProgressDialog progressDialog = Utils.progressDialog("Loading. Please wait...");

        if (progressDialog != null)
            progressDialog.show();

        JsonObjectRequest req = new JsonObjectRequest(method, url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if (progressDialog != null)
                            progressDialog.dismiss();

                        Log.e("Response:", response.toString());
                        mVolleyResponse.onVolleyResponse(url, requestTag, response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


                if (progressDialog != null)
                    progressDialog.dismiss();

                Log.e("Error: ", error.getMessage() + "test");
                mVolleyResponse.onVolleyError(url, requestTag, error);
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        req.setShouldCache(false);
        BaseApplication.getInstance().addToRequestQueue(req, requestTag);
    }

    public void makeJsonArrayRequest(final String requestTag, final String url, int method, JSONArray mJsonArray) {

        Log.e("Params", mJsonArray.toString());

        final ProgressDialog progressDialog = Utils.progressDialog("Loading. Please wait...");

        if (progressDialog != null)
            progressDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(method, url, mJsonArray,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        if (progressDialog != null)
                            progressDialog.dismiss();

                        Log.e("Response:", response.toString());

                        JSONObject mJsonObject1 = new JSONObject();
                        try {
                            mJsonObject1.put("length", response.length());
                            mJsonObject1.put("data", response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mVolleyResponse.onVolleyResponse(url, requestTag, mJsonObject1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (progressDialog != null)
                    progressDialog.dismiss();

                Log.e("Error: ", error.getMessage() + "test");
                mVolleyResponse.onVolleyError(url, requestTag, error);
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        req.setShouldCache(false);
        BaseApplication.getInstance().addToRequestQueue(req, requestTag);
    }

    public void makeMultipartRequestSingleFile(final int item_pos, final String filePath, final String requestTag, final String url, int method) {


        Log.e("filePath", filePath);
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response: ", response + "");
                        try {
                            JSONObject jObj = new JSONObject(response);
                            mVolleyResponse.onVolleyResponse(url, requestTag, jObj);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error: ", error.getMessage() + "");
                mVolleyResponse.onVolleyError(url, requestTag, error);
            }
        });

        smr.addFile("file", filePath);
        smr.setFixedStreamingMode(true);

        smr.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        BaseApplication.getInstance().addToRequestQueue(smr);
    }

    public void makeMultipartRequestMultipleFiles(final List<File> images_path, final String requestTag, final String url, int method) {


        Log.e("images_path", images_path.toString());

        String[] files = new String[images_path.size()];
        for (int i = 0; i < images_path.size(); i++) {
            files[i] = images_path.get(i).getAbsolutePath();
        }

        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(method, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response: ", response + "onResponse");
                        try {
                            JSONObject jObj = new JSONObject(response);
                            mVolleyResponse.onVolleyResponse(url, requestTag, jObj);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error: ", error.getMessage() + "Error");
                mVolleyResponse.onVolleyError(url, requestTag, error);
            }
        }) {
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {

                Log.e("parseNetworkResponse", response.data.toString());

                if (response.headers == null) {
                    // cant just set a new empty map because the member is final.
                    Log.e("if", response.statusCode + "");
                    response = new NetworkResponse(
                            response.statusCode,
                            response.data,
                            Collections.<String, String>emptyMap(), // this is the important line, set an empty but non-null map.
                            response.notModified,
                            response.networkTimeMs);
                }

                return super.parseNetworkResponse(response);
            }
        };

        for (int i = 0; i < images_path.size(); i++) {
            smr.addFile("files[" + i + "]", images_path.get(i).getAbsolutePath());
        }
        smr.setFixedStreamingMode(true);
        smr.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        BaseApplication.getInstance().addToRequestQueue(smr);
    }


    public interface VolleyResponse {

        public abstract void onVolleyResponse(String url, String tag, JSONObject response);

        public abstract void onVolleyError(String url, String tag, VolleyError error);
    }

}
