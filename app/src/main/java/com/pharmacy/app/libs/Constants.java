package com.pharmacy.app.libs;

public class Constants {

    public static final String KEY_PDF_DOC = "PDF_DOC";
    public static final String KEY_VIDEO_FILE = "VIDEO_FILE";
    public static final String KEY_AUDIO_FILE = "AUDIO_FILE";
    public static final String KEY_UNKNOWN_MEDIA_FILE = "UNKNOWN_MEDIA_FILE";
    public static final String KEY_CAMERA_TYPE_NATVE = "native";
    public static final String KEY_CAMERA_TYPE_LIBRARY = "library";
    public static final String KEY_FILE_PROVIDER_ID = "enact.cit.app.provider";

    public static final int TAB_ACTIVITY_SUMMARY = 0;
    public static final int TAB_ACTIVITIES_PERFORMED = 1;
    public static final int TAB_ACTIVITIES_UPLOADING = 2;
    public static final int TAB_ACTIVITIES_APPROVAL_STATUS = 3;

    public static final String VIEW_TAG_CHECKBOX_CONTAINER = "CHECKBOX_CONTAINER";
    public static final String VIEW_TAG_RADIOGROUP_CONTAINER = "RADIOGROUP_CONTAINER";
    public static final String VIEW_TAG_DROPDOWN_CONTAINER = "DROPDOWN_CONTAINER";
    public static final String VIEW_TAG_INPUTFIELD_CONTAINER = "INPUT_CONTAINER";
    public static final String VIEW_TAG_STARRATING_CONTAINER = "STARRATING_CONTAINER";

}
