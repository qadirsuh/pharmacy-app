package com.pharmacy.app.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.pharmacy.app.R;
import com.pharmacy.app.models.Pharmacy;

public class PharmacyAdapter extends BaseAdapter {

    private Context context;
    private Pharmacy[] mPharmacies;

    public PharmacyAdapter(Context context, Pharmacy[] mPharmacies) {
        this.context = context;
        this.mPharmacies = mPharmacies;
    }

    @Override
    public int getCount() {
        return mPharmacies.length;
    }

    @Override
    public Object getItem(int position) {
        return mPharmacies[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.layout_pharmacy_items, null);
        }

        //  ImageView img_campaign_image = (ImageView) convertView.findViewById(R.id.img_campaign_image);
        TextView txt_name = (TextView) convertView.findViewById(R.id.txt_name);
        TextView txt_desc = (TextView) convertView.findViewById(R.id.txt_desc);

        txt_name.setText(mPharmacies[position].title);
        txt_desc.setText(mPharmacies[position].desc);
        txt_name.setSelected(true);

        //  String img_url = APIs.getImageUrl(mCampaigns[position].img1);
        /*Picasso.with(context)
                .load(img_url)
                .error(R.drawable.ic_image_error)
                .placeholder(R.drawable.img_placeholder)
                .into(img_campaign_image);*/

        return convertView;
    }

}