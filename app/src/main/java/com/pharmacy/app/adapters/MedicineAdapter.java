package com.pharmacy.app.adapters;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.models.Medicine;
import com.squareup.picasso.Picasso;

public class MedicineAdapter extends BaseAdapter {

    private Context context;
    private Medicine[] medicines;

    public MedicineAdapter(Context context, Medicine[] medicines) {
        this.context = context;
        this.medicines = medicines;
    }

    @Override
    public int getCount() {
        return medicines.length;
    }

    @Override
    public Object getItem(int position) {
        return medicines[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.layout_medicine_items, null);
        }

        ImageView img_medicine_image = (ImageView) convertView.findViewById(R.id.img_medicine_image);
        TextView txt_medicine_name = (TextView) convertView.findViewById(R.id.txt_medicine_name);
        TextView txt_expiry = (TextView) convertView.findViewById(R.id.txt_expiry);

        txt_medicine_name.setText(medicines[position].title);
        txt_expiry.setText(medicines[position].edate);
        txt_medicine_name.setSelected(true);

        String img_url = APIs.getImageUrl(medicines[position].img);

        Picasso.with(context)
                .load(img_url)
                .error(R.drawable.ic_image_error)
                .placeholder(R.drawable.img_placeholder)
                .into(img_medicine_image);

        return convertView;
    }

}