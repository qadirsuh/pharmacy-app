package com.pharmacy.app.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.gson.Gson;
import com.pharmacy.app.R;
import com.pharmacy.app.adapters.PharmacyAdapter;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Pharmacy;

import org.json.JSONArray;
import org.json.JSONObject;

public class ViewPharmaciesActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();

    private PharmacyAdapter mPharmacyAdapter;
    private SwipeRefreshLayout swipe_refresh_campaign_listing;
    private ListView mListView;

    private boolean isAdmin = false;

    private Button btn_logout;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_view_pharmacies;
    }

    @Override
    protected void initViews() {

        mListView = (ListView) findViewById(R.id.listview_pharmacies);
        swipe_refresh_campaign_listing = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_campaign_listing);
        btn_logout = findViewById(R.id.btn_logout);

    }

    @Override
    protected void manipulateViews() {
        mVolleyWebservice.setVolleyLister(this);

        isAdmin = getIntent().getBooleanExtra("isAdmin", true);

        if (isAdmin) {
            btn_logout.setVisibility(View.GONE);
        }

        swipe_refresh_campaign_listing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_campaign_listing.setRefreshing(false);
                callPharmacyListingApi();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {


                Pharmacy mPharmacy = (Pharmacy) adapterView.getItemAtPosition(position);

                if (isAdmin) {
                    Intent mIntent = new Intent(ViewPharmaciesActivity.this, AddPharmacyActivity.class);
                    mIntent.putExtra("isUpdatePharmacy", true);
                    mIntent.putExtra("PharmacyData", mPharmacy);
                    startActivity(mIntent);
                } else {

                    Intent mIntent = new Intent(ViewPharmaciesActivity.this, ViewBranchesActivity.class);
                    mIntent.putExtra("PharmacyData", mPharmacy);
                    startActivity(mIntent);
                }
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.clearAllLoginSharePref();
                Intent mIntent = new Intent(ViewPharmaciesActivity.this, LoginMenuActivity.class);
                startActivity(mIntent);
                Toast.makeText(ViewPharmaciesActivity.this, "Logout Success!", Toast.LENGTH_SHORT).show();
                finish();

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Call View Pharmacies API here
        callPharmacyListingApi();
    }

    private void callPharmacyListingApi() {

        String[] url = APIs.getPharmaciesAPIUrl();
        mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());

    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        Log.e("onVolley Pharmacies", response.toString());

        try {
            if (response.length() != 0) {

                Gson gson = new Gson();
                Pharmacy[] mPharmacies = gson.fromJson(response.getJSONArray("data").toString(), Pharmacy[].class);
                Log.e("mPharmacies", mPharmacies[0].title);
                mPharmacyAdapter = new PharmacyAdapter(ViewPharmaciesActivity.this, mPharmacies);
                mListView.setAdapter(mPharmacyAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
