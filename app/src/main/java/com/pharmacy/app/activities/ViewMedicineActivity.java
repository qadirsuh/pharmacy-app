package com.pharmacy.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.gson.Gson;
import com.pharmacy.app.R;
import com.pharmacy.app.adapters.MedicineAdapter;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Medicine;
import com.pharmacy.app.models.Pharmacy;
import com.pharmacy.app.models.PharmacyBranch;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewMedicineActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();

    private SwipeRefreshLayout swipe_refresh_campaign_listing;
    private ListView mListView;

    private Intent mIntent;

    private MedicineAdapter medicineAdapter;


    //    private Medicine selectedMedicine;
    private String selectedBranchId = "-1";

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_view_medicine;


    }

    @Override
    protected void initViews() {

        mListView = (ListView) findViewById(R.id.listview_medicines);
        swipe_refresh_campaign_listing = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_campaign_listing);
    }

    @Override
    protected void onResume() {
        super.onResume();
        callMedicineListingApi(selectedBranchId);
    }

    @Override
    protected void manipulateViews() {

        mVolleyWebservice.setVolleyLister(this);

        mIntent = getIntent();
        selectedBranchId = mIntent.getStringExtra("selectedBranchId");

        swipe_refresh_campaign_listing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_campaign_listing.setRefreshing(false);
                callMedicineListingApi(selectedBranchId);
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {


                Medicine mMedicine = (Medicine) adapterView.getItemAtPosition(position);
                Toast.makeText(ViewMedicineActivity.this, mMedicine.title, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void callMedicineListingApi(String selectedPharmacyId) {

        // Call Get pharmacies Branches API here
        String[] url = APIs.getMedicinesAPIUrl(selectedPharmacyId);
        mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());
    }


    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        try {
            if (response.length() != 0) {

                Gson gson = new Gson();
                Medicine[] mMedicines = gson.fromJson(response.getJSONArray("data").toString(), Medicine[].class);
                Log.e("pharmacyBranches", mMedicines[0].title);

                medicineAdapter = new MedicineAdapter(ViewMedicineActivity.this, mMedicines);
                mListView.setAdapter(medicineAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
