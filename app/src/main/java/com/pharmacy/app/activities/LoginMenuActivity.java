package com.pharmacy.app.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.greysonparrelli.permiso.Permiso;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.Utils;

import static com.pharmacy.app.BaseApplication.setCurrentActivity;


public class LoginMenuActivity extends BaseActivity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private TextView heading_text;
    private Button btn_login_as_admin;
    private Button btn_login_as_user;
    private LinearLayout lin_login_menu;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login_menu;
    }


    @Override
    protected void initViews() {
        Log.e("initViews", "called");
        heading_text = findViewById(R.id.heading_text);
        btn_login_as_admin = findViewById(R.id.btn_login_as_admin);
        btn_login_as_user = findViewById(R.id.btn_login_as_user);
        lin_login_menu = findViewById(R.id.lin_login_menu);
    }

    @Override
    protected void manipulateViews() {
        Permiso.getInstance().setActivity(this);

        btn_login_as_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(LoginMenuActivity.this, LoginActivity.class);
                mIntent.putExtra("LoginAs", "ADMIN");
                startActivity(mIntent);
                finish();
            }
        });

        btn_login_as_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(LoginMenuActivity.this, LoginActivity.class);
                mIntent.putExtra("LoginAs", "USER");
                startActivity(mIntent);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //int ACCESS_FINE_LOCATION = checkSelfPermission("android.permission.ACCESS_FINE_LOCATION");
            int CAMERA = checkSelfPermission("android.permission.CAMERA");
            //int READ_EXTERNAL_STORAGE = checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE");

            if (CAMERA == 0) {
                heading_text.setText("Pharmacy App\nLogin Menu");
                lin_login_menu.setVisibility(View.VISIBLE);
            } else {
                heading_text.setText("App Permissions Required to continue");

                heading_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        takeAppPermissions();
                    }
                });
            }

        }

        setCurrentActivity(this);

        Permiso.getInstance().setActivity(this);
        takeAppPermissions();
    }

    private void takeAppPermissions() {

        Log.e("takeAppPermissions", "called");

        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
            @Override
            public void onPermissionResult(Permiso.ResultSet resultSet) {

                if (resultSet.isPermissionGranted(Manifest.permission.CAMERA)) {
                    // CAMERA permission granted!
                }

                if (resultSet.isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // WRITE_EXTERNAL_STORAGE permission granted!
                }

                Log.e("AllPermissionsGranted", resultSet.areAllPermissionsGranted() + "");

                if (!resultSet.areAllPermissionsGranted()) {
                    takeAppPermissions();
                } else {
                    checkIfAlreadyLogin();
                }
            }

            @Override
            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                Permiso.getInstance().showRationaleInDialog("All App permissions are required!", "App won't function without these permissions", null, callback);
            }
        }, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE);

    }

    private void checkIfAlreadyLogin() {

        Log.e("checkIfAlreadyLogin", "called");
        String id = Utils.getSharedPrefRef("id");
        String name = Utils.getSharedPrefRef("name");
        String email = Utils.getSharedPrefRef("email");
        String phone = Utils.getSharedPrefRef("phone");
        String status = Utils.getSharedPrefRef("status");
        boolean isAdminLogin = Utils.getSharedPrefRefboolean("isAdminLogin");

        if (id == null) {

            // No body is logged in

//            Intent i = new Intent(LoginMenuActivity.this, LoginActivity.class);
//            startActivity(i);
//            finish();
//            Log.e("id", "null");

            lin_login_menu.setVisibility(View.VISIBLE);

        } else {

            if (isAdminLogin) {
                // Admin already logged in
                Intent i = new Intent(LoginMenuActivity.this, AdminDashboardActivity.class);
                startActivity(i);
                finish();
            } else {

                // UserLogin already Logged in
                Intent mIntent = new Intent(LoginMenuActivity.this, ViewPharmaciesActivity.class);
                mIntent.putExtra("isAdmin", false);
                startActivity(mIntent);
                finish();
            }

            Log.e("id", "!null");
        }
    }
}
