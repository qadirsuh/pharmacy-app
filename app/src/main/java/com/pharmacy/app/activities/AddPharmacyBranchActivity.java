package com.pharmacy.app.activities;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.gson.Gson;
import com.pharmacy.app.R;
import com.pharmacy.app.adapters.PharmacyAdapter;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Pharmacy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddPharmacyBranchActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private Spinner spnr_pharmacies;
    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();
    private ArrayList<String> pharmaciesNames;
    private ArrayList<String> pharmaciesIds;

    private String selectedPharmacyId;
    private String selectedPharmacyName;

    private EditText ed_name, ed_phone, ed_email, ed_desc;
    private Button btn_submit;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_pharmacy_branch;
    }

    @Override
    protected void initViews() {

        spnr_pharmacies = findViewById(R.id.spnr_pharmacies);

        ed_name = findViewById(R.id.ed_name);
        ed_phone = findViewById(R.id.ed_phone);
        ed_email = findViewById(R.id.ed_email);
        ed_desc = findViewById(R.id.ed_desc);
        btn_submit = findViewById(R.id.btn_submit);
    }

    @Override
    protected void manipulateViews() {

        mVolleyWebservice.setVolleyLister(this);
        spnr_pharmacies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPharmacyId = pharmaciesIds.get(position);
                selectedPharmacyName = pharmaciesNames.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = ed_name.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phone.getText().toString();
                String description = ed_desc.getText().toString();

                if (selectedPharmacyId.equalsIgnoreCase("-1")) {
                    Toast.makeText(AddPharmacyBranchActivity.this, "Error! Please select pharmacy.", Toast.LENGTH_SHORT).show();
                } else if (name.isEmpty()) {
                    Utils.setError(ed_name, "Field Required");
                } else if (email.isEmpty()) {
                    Utils.setError(ed_email, "Field Required");
                } else if (phone.isEmpty()) {
                    Utils.setError(ed_phone, "Field Required");
                } else if (description.isEmpty()) {
                    Utils.setError(ed_desc, "Field Required");
                } else {

                    // Call Registration API here
                    String[] url = APIs.getAddPharmacyBranchUrl(selectedPharmacyId, name, phone, email, description);
                    mVolleyWebservice.makeJsonObjectRequest(url[0], url[1], Request.Method.GET, new JSONObject());
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        // Call Get pharmacies API here
        String[] url = APIs.getPharmaciesAPIUrl();
        mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());

    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {


        Log.e("BranchActivity", response.toString());

        if (tag.equalsIgnoreCase("getPharmaciesAPIUrl")) {

            try {
                if (response.length() != 0) {

                    Gson gson = new Gson();
                    Pharmacy[] mPharmacies = gson.fromJson(response.getJSONArray("data").toString(), Pharmacy[].class);
                    Log.e("mPharmacies", mPharmacies[0].title);

                    pharmaciesNames = new ArrayList<>();
                    pharmaciesNames.add("Select Pharmacy");
                    pharmaciesIds = new ArrayList<>();
                    pharmaciesIds.add("-1");
                    for (int i = 0; i < mPharmacies.length; i++) {
                        pharmaciesNames.add(mPharmacies[i].title);
                        pharmaciesIds.add(mPharmacies[i].id);
                    }

                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, pharmaciesNames);
                    mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnr_pharmacies.setAdapter(mArrayAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

            try {
                boolean status = response.getBoolean("status");

                String message = "";
                if (response.has("message")) {
                    message = response.getString("message");
                }

                if (status == APIs.STATUS_SUCCESS) {
                    Toast.makeText(AddPharmacyBranchActivity.this, "Successfully Added!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(AddPharmacyBranchActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(AddPharmacyBranchActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
