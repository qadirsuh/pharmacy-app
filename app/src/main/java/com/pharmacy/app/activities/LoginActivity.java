package com.pharmacy.app.activities;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.pharmacy.app.BaseApplication;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.UserLogin;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class LoginActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private EditText ed_email, ed_password;
    private Button btn_submit;
    private TextView txt_register;
    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();
    private Intent mIntent;
    private boolean isAdminLogin = false;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {

        ed_email = findViewById(R.id.ed_email);
        ed_password = findViewById(R.id.ed_password);
        btn_submit = findViewById(R.id.btn_submit);
        txt_register = findViewById(R.id.txt_register);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void manipulateViews() {

        mVolleyWebservice.setVolleyLister(this);
        mIntent = getIntent();

        if (mIntent.getStringExtra("LoginAs").equals("ADMIN")) {
            txt_register.setVisibility(View.GONE);
            isAdminLogin = true;
        } else {
            isAdminLogin = false;
        }

        txt_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(mIntent);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = ed_email.getText().toString();
                String password = ed_password.getText().toString();

                if (email.isEmpty()) {
                    Utils.setError(ed_email, "Field Required");
                } else if (password.isEmpty()) {
                    Utils.setError(ed_password, "Field Required");
                } else {

                    // Call Login API here
                    String[] url = APIs.getLoginApi(email, password, isAdminLogin);
                    mVolleyWebservice.makeJsonObjectRequest(url[0], url[1], Request.Method.GET, new JSONObject());
                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        Log.e("Login response", response.toString());

        try {
            Boolean status = response.getBoolean("status");

            String message = "";
            if (response.has("message")) {
                message = response.getString("message");
            }

            if (status == APIs.STATUS_SUCCESS) {

                Toast.makeText(LoginActivity.this, "Successfully Logged in!", Toast.LENGTH_SHORT).show();

                // Save Data to SharedPref
                JSONObject data = response.getJSONObject("0");

                String id = data.getString("id");
                String name = data.getString("name");
                String email = data.getString("email");
                String phone = data.getString("phone");
                String status1 = data.getString("status");
                String loginDate = new Date().toLocaleString();

                UserLogin mUserLogin = new UserLogin(id, name, email, phone, status1, isAdminLogin, loginDate);
                saveLoginUser(mUserLogin);

                if (isAdminLogin) {
                    Intent mIntent = new Intent(LoginActivity.this, AdminDashboardActivity.class);
                    startActivity(mIntent);

                    if (!BaseApplication.isRegisteredNotificationLister) {
                        BaseApplication.registerNotificationForAdmin();
                    }


                } else {
                    Intent mIntent = new Intent(LoginActivity.this, ViewPharmaciesActivity.class);
                    mIntent.putExtra("isAdmin", false);
                    startActivity(mIntent);

                    // Push notification entry if not admin
                    notifyAdmin(mUserLogin);
                }

                finish();

            } else {

                // Utils.showFailInfoAlert("Failed to create account!", message, "OK", null);
                Toast.makeText(LoginActivity.this, "Invalid email/password!", Toast.LENGTH_SHORT).show();
                Utils.setError(ed_email, "Invalid email/password");
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }

    private void notifyAdmin(UserLogin mUserLogin) {

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        rootRef.child("userLoginNotification").push().setValue(mUserLogin);

    }

    private void saveLoginUser(UserLogin mUserLogin) {

        Utils.saveSharedPrefRef("id", mUserLogin.id);
        Utils.saveSharedPrefRef("name", mUserLogin.name);
        Utils.saveSharedPrefRef("email", mUserLogin.email);
        Utils.saveSharedPrefRef("phone", mUserLogin.phone);
        Utils.saveSharedPrefRef("status", mUserLogin.status);
        Utils.saveSharedPrefRef("isAdminLogin", isAdminLogin);

    }
}
