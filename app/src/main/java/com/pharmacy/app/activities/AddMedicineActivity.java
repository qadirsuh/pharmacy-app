package com.pharmacy.app.activities;


import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.gson.Gson;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.ImageOptimize;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Pharmacy;
import com.pharmacy.app.models.PharmacyBranch;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class AddMedicineActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();

    private ImageView img_upload_image;
    private final int CAMERA_REQUEST = 111;
    private ImageOptimize mImageOptimize;

    private ProgressBar progressbar_upload_image;
    private String uploaded_image_url = "";

    private ArrayList<String> pharmaciesNames;
    private ArrayList<String> pharmaciesIds;

    private String selectedPharmacyId = "-1";
    private String selectedPharmacyName;
    private Spinner spnr_pharmacies;
    private Spinner spnr_branches;

    private String selectedBranchId = "-1";
    private String selectedBranchName;

    private ArrayList<String> branchNames;
    private ArrayList<String> branchIds;

    private boolean isFirstTimeAutoSelectPharmacy = true;

    private EditText ed_name, ed_price, ed_date_manufacture, ed_date_expiry, ed_desc;
    private Button btn_submit;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_medicine;
    }

    @Override
    protected void initViews() {
        spnr_pharmacies = findViewById(R.id.spnr_pharmacies);
        spnr_branches = findViewById(R.id.spnr_branches);
        img_upload_image = findViewById(R.id.img_upload_image);
        progressbar_upload_image = findViewById(R.id.progressbar_upload_image);

        ed_name = findViewById(R.id.ed_name);
        ed_price = findViewById(R.id.ed_price);
        ed_date_manufacture = findViewById(R.id.ed_date_manufacture);
        ed_date_expiry = findViewById(R.id.ed_date_expiry);
        ed_desc = findViewById(R.id.ed_desc);

        btn_submit = findViewById(R.id.btn_submit);
    }

    @Override
    protected void manipulateViews() {
        mVolleyWebservice.setVolleyLister(this);
        mImageOptimize = new ImageOptimize(this);

        img_upload_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openChooserWithGallery(AddMedicineActivity.this, "Select One", CAMERA_REQUEST);
            }
        });

        spnr_pharmacies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedPharmacyId = pharmaciesIds.get(position);
                selectedPharmacyName = pharmaciesNames.get(position);


                Log.e("isFirst", isFirstTimeAutoSelectPharmacy + "");

                if (!isFirstTimeAutoSelectPharmacy && !selectedPharmacyId.equalsIgnoreCase("-1")) {

                    // Call Get pharmacies Branches API here
                    String[] url = APIs.getPharmacyBranchesAPIUrl(selectedPharmacyId);
                    mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());
                    isFirstTimeAutoSelectPharmacy = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnr_branches.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedBranchId = branchIds.get(position);
                selectedBranchName = branchNames.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Call Get pharmacies API here
        String[] url = APIs.getPharmaciesAPIUrl();
        mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = ed_name.getText().toString();
                String price = ed_price.getText().toString();
                String date_manuf = ed_date_manufacture.getText().toString();
                String date_expiry = ed_date_expiry.getText().toString();
                String description = ed_desc.getText().toString();

                if (selectedPharmacyId.equalsIgnoreCase("-1")) {
                    Toast.makeText(AddMedicineActivity.this, "Error! Please select Pharmacy.", Toast.LENGTH_SHORT).show();
                } else if (selectedBranchId.equalsIgnoreCase("-1")) {
                    Toast.makeText(AddMedicineActivity.this, "Error! Please select Branch.", Toast.LENGTH_SHORT).show();
                } else if (name.isEmpty()) {
                    Utils.setError(ed_name, "Field Required");
                } else if (price.isEmpty()) {
                    Utils.setError(ed_price, "Field Required");
                } else if (date_manuf.isEmpty()) {
                    Utils.setError(ed_date_manufacture, "Field Required");
                } else if (date_expiry.isEmpty()) {
                    Utils.setError(ed_date_expiry, "Field Required");
                } else if (description.isEmpty()) {
                    Utils.setError(ed_desc, "Field Required");
                } else if (uploaded_image_url.isEmpty()) {
                    Toast.makeText(AddMedicineActivity.this, "Error! Please upload image first.", Toast.LENGTH_SHORT).show();
                } else {

                    // Call add Medicine API here
                    String[] url = APIs.getAddMedicineUrl(selectedPharmacyId, selectedBranchId, name, price, date_manuf, date_expiry, description, uploaded_image_url);
                    mVolleyWebservice.makeJsonObjectRequest(url[0], url[1], Request.Method.GET, new JSONObject());
                }
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling

                if (type == CAMERA_REQUEST) {
                    e.printStackTrace();
                    Log.e("onImagePickerError", e.getMessage());
                }
            }

            @Override
            public void onImagesPicked(List<File> imagesFiles, EasyImage.ImageSource source, int type) {
                //Handle the images
                //onPhotosReturned(imagesFiles);

                if (type == CAMERA_REQUEST) {
                    Log.e("imagesFiles", imagesFiles.toString());

                    File fileToUpload = imagesFiles.get(0);
                    String optimizedImagePath = mImageOptimize.compressImage(fileToUpload.getPath());
                    Log.e("optimizedImagePath", optimizedImagePath);

                    File optimizedFile = new File(optimizedImagePath);
                    img_upload_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    Picasso.with(AddMedicineActivity.this).load(optimizedFile).into(img_upload_image);

                    List<File> imgTemp = new ArrayList<>();
                    imgTemp.add(optimizedFile);
                    callUploadImageApi(imgTemp);
                }
            }
        });
    }

    private void callUploadImageApi(List<File> imagesFiles) {

        progressbar_upload_image.setVisibility(View.VISIBLE);
        btn_submit.setEnabled(false);
        String[] url = APIs.getUploadImageUrl();
        mVolleyWebservice.makeMultipartRequestMultipleFiles(imagesFiles, url[0], url[1], Request.Method.POST);

    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        if (tag.equalsIgnoreCase("UploadImage")) {

            progressbar_upload_image.setVisibility(View.GONE);
            btn_submit.setEnabled(true);

            try {
                String status = response.getString("status");

                String message = "";
                if (response.has("message")) {
                    message = response.getString("message");
                }

                if (status.equalsIgnoreCase("true")) {
                    JSONArray raw = response.getJSONArray("image_paths");
                    uploaded_image_url = raw.get(0).toString();

                    Log.e("uploaded_image_url", uploaded_image_url);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (tag.equalsIgnoreCase("getPharmaciesAPIUrl")) {

            try {
                if (response.length() != 0) {

                    Gson gson = new Gson();
                    Pharmacy[] mPharmacies = gson.fromJson(response.getJSONArray("data").toString(), Pharmacy[].class);
                    Log.e("mPharmacies", mPharmacies[0].title);

                    pharmaciesNames = new ArrayList<>();
                    pharmaciesNames.add("Select Pharmacy");
                    pharmaciesIds = new ArrayList<>();
                    pharmaciesIds.add("-1");
                    for (int i = 0; i < mPharmacies.length; i++) {
                        pharmaciesNames.add(mPharmacies[i].title);
                        pharmaciesIds.add(mPharmacies[i].id);
                    }

                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, pharmaciesNames);
                    mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnr_pharmacies.setAdapter(mArrayAdapter);

                    isFirstTimeAutoSelectPharmacy = false;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (tag.equalsIgnoreCase("getPharmacyBranches")) {


            try {
                if (response.length() != 0) {

                    Gson gson = new Gson();
                    PharmacyBranch[] pharmacyBranches = gson.fromJson(response.getJSONArray("data").toString(), PharmacyBranch[].class);
                    Log.e("pharmacyBranches", pharmacyBranches[0].title);

                    branchNames = new ArrayList<>();
                    branchNames.add("Select Branch");
                    branchIds = new ArrayList<>();
                    branchIds.add("-1");
                    for (int i = 0; i < pharmacyBranches.length; i++) {
                        branchNames.add(pharmacyBranches[i].title);
                        branchIds.add(pharmacyBranches[i].id);
                    }

                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, branchNames);
                    mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spnr_branches.setAdapter(mArrayAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (tag.equalsIgnoreCase("getAddMedicineUrl")) {


            try {
                boolean status = response.getBoolean("status");

                String message = "";
                if (response.has("message")) {
                    message = response.getString("message");
                }

                if (status == APIs.STATUS_SUCCESS) {
                    Toast.makeText(AddMedicineActivity.this, "Successfully Added!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(AddMedicineActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(AddMedicineActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

        if (tag.equalsIgnoreCase("getPharmacyBranches")) {
            Toast.makeText(this, "No branches found! \nPlease add one to continue", Toast.LENGTH_LONG).show();
        }
    }
}
