package com.pharmacy.app.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.google.gson.Gson;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Pharmacy;
import com.pharmacy.app.models.PharmacyBranch;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ViewBranchesActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();
    private SwipeRefreshLayout swipe_refresh_campaign_listing;
    private ListView mListView;

    private Pharmacy selectedPharmacy;

    private String selectedBranchId = "-1";
    private String selectedBranchName;

    private ArrayList<String> branchNames;
    private ArrayList<String> branchIds;

    private Intent mIntent;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_view_branches;
    }

    @Override
    protected void initViews() {

        mListView = (ListView) findViewById(R.id.listview_branches);
        swipe_refresh_campaign_listing = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_campaign_listing);

    }

    @Override
    protected void manipulateViews() {

        mVolleyWebservice.setVolleyLister(this);
        mIntent = getIntent();
        selectedPharmacy = (Pharmacy) mIntent.getSerializableExtra("PharmacyData");

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                String selectedBranchId = branchIds.get(position);
                Log.e("selectedBranchId", selectedBranchId);
                Intent mIntent = new Intent(ViewBranchesActivity.this, ViewMedicineActivity.class);
                mIntent.putExtra("selectedBranchId", selectedBranchId);
                startActivity(mIntent);
            }
        });

        swipe_refresh_campaign_listing.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_campaign_listing.setRefreshing(false);
                callBranchListingApi(selectedPharmacy.id);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        callBranchListingApi(selectedPharmacy.id);
    }

    private void callBranchListingApi(String selectedPharmacyId) {

        // Call Get pharmacies Branches API here
        String[] url = APIs.getPharmacyBranchesAPIUrl(selectedPharmacyId);
        mVolleyWebservice.makeJsonArrayRequest(url[0], url[1], Request.Method.GET, new JSONArray());
    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        if (tag.equalsIgnoreCase("getPharmacyBranches")) {


            try {
                if (response.length() != 0) {

                    Gson gson = new Gson();
                    PharmacyBranch[] pharmacyBranches = gson.fromJson(response.getJSONArray("data").toString(), PharmacyBranch[].class);
                    Log.e("pharmacyBranches", pharmacyBranches[0].title);

                    branchNames = new ArrayList<>();
                    branchIds = new ArrayList<>();

                    for (int i = 0; i < pharmacyBranches.length; i++) {
                        branchNames.add(pharmacyBranches[i].title);
                        branchIds.add(pharmacyBranches[i].id);
                    }

                    ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, branchNames);
                    //mArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mListView.setAdapter(mArrayAdapter);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
