package com.pharmacy.app.activities;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;

import org.json.JSONObject;

public class RegistrationActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {


    private EditText ed_name, ed_email, ed_phone, ed_password;
    private Button btn_submit;
    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();

//    public static String phone_number = "";

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_registration;
    }

    @Override
    protected void initViews() {

        ed_name = findViewById(R.id.ed_name);
        ed_email = findViewById(R.id.ed_email);
        ed_phone = findViewById(R.id.ed_phone);
        ed_password = findViewById(R.id.ed_password);
        btn_submit = findViewById(R.id.btn_submit);

    }

    @Override
    protected void manipulateViews() {

        mVolleyWebservice.setVolleyLister(this);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = ed_name.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phone.getText().toString();
                String password = ed_password.getText().toString();

                if (name.isEmpty()) {
                    Utils.setError(ed_name, "Field Required");
                } else if (email.isEmpty()) {
                    Utils.setError(ed_email, "Field Required");
                } else if (phone.isEmpty()) {
                    Utils.setError(ed_phone, "Field Required");
                } else if (password.isEmpty()) {
                    Utils.setError(ed_password, "Field Required");
                } else {

                    // Call Registration API here
                    String[] url = APIs.getRegisterApi(name, email, phone, password);
                    mVolleyWebservice.makeJsonObjectRequest(url[0], url[1], Request.Method.GET, new JSONObject());
                }
            }
        });
    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        Log.e("Reg Response", response.toString());

        try {
            Boolean status = response.getBoolean("status");

            String message = "";
            if (response.has("message")) {
                message = response.getString("message");
            }

            if (status == APIs.STATUS_SUCCESS) {

                Runnable mRunnable = new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                };

                try {
                    Utils.showSuccessAlert("Success!", message + "\nYou will be redirected to Login screen now.", "OK", mRunnable);
                } catch (Exception e) {
                    Toast.makeText(RegistrationActivity.this, "Successfully Registered!", Toast.LENGTH_SHORT).show();
                    finish();
                }

            } else {

                try {
                    Utils.showFailInfoAlert("Failed to create account!", message, "OK", null);
                } catch (Exception e) {
                    Toast.makeText(RegistrationActivity.this, "Failed to create account!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(RegistrationActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
