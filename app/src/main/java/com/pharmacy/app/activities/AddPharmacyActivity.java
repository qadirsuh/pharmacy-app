package com.pharmacy.app.activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.error.VolleyError;
import com.pharmacy.app.R;
import com.pharmacy.app.libs.APIs;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.libs.VolleyWebservice;
import com.pharmacy.app.models.Pharmacy;

import org.json.JSONObject;

public class AddPharmacyActivity extends BaseActivity implements VolleyWebservice.VolleyResponse {

    private EditText ed_name, ed_phone, ed_email, ed_desc;
    private Button btn_submit;
    private TextView txt_add_update_pharmacy;

    private VolleyWebservice mVolleyWebservice = new VolleyWebservice();
    private Intent mIntent;
    private boolean isUpdatePharmacy = false;
    Pharmacy mPharmacy;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_add_pharmacy;
    }

    @Override
    protected void initViews() {

        txt_add_update_pharmacy = findViewById(R.id.txt_add_update_pharmacy);
        ed_name = findViewById(R.id.ed_name);
        ed_phone = findViewById(R.id.ed_phone);
        ed_email = findViewById(R.id.ed_email);
        ed_desc = findViewById(R.id.ed_desc);
        btn_submit = findViewById(R.id.btn_submit);

    }

    @Override
    protected void manipulateViews() {
        mVolleyWebservice.setVolleyLister(this);

        mIntent = getIntent();
        isUpdatePharmacy = mIntent.getBooleanExtra("isUpdatePharmacy", false);

        if (isUpdatePharmacy) {

            // Set values
            txt_add_update_pharmacy.setText("Update Pharmacy");
            mPharmacy = (Pharmacy) mIntent.getSerializableExtra("PharmacyData");
            Log.e("mPharmacy", mPharmacy.title);
            ed_name.setText(mPharmacy.title);
            ed_email.setText(mPharmacy.email);
            ed_phone.setText(mPharmacy.phone);
            ed_desc.setText(mPharmacy.desc);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = ed_name.getText().toString();
                String email = ed_email.getText().toString();
                String phone = ed_phone.getText().toString();
                String description = ed_desc.getText().toString();

                if (name.isEmpty()) {
                    Utils.setError(ed_name, "Field Required");
                } else if (email.isEmpty()) {
                    Utils.setError(ed_email, "Field Required");
                } else if (phone.isEmpty()) {
                    Utils.setError(ed_phone, "Field Required");
                } else if (description.isEmpty()) {
                    Utils.setError(ed_desc, "Field Required");
                } else {

                    // Call Add / Update API here
                    String[] url;
                    if (!isUpdatePharmacy) {
                        url = APIs.getAddPharmacyUrl(name, phone, email, description);
                    } else {
                        // update api url
                        url = APIs.getUpdatePharmacyUrl(mPharmacy.id, name, phone, email, description);
                    }

                    mVolleyWebservice.makeJsonObjectRequest(url[0], url[1], Request.Method.GET, new JSONObject());
                }
            }
        });

    }

    @Override
    public void onVolleyResponse(String url, String tag, JSONObject response) {

        Log.e("Add Pharmacy resp", response.toString());

        try {
            Boolean status = response.getBoolean("status");

            String message = "";
            if (response.has("message")) {
                message = response.getString("message");
            }

            if (status == APIs.STATUS_SUCCESS) {

                if (!isUpdatePharmacy) {
                    Toast.makeText(AddPharmacyActivity.this, "Successfully Added!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddPharmacyActivity.this, "Successfully Update!", Toast.LENGTH_SHORT).show();
                }

                finish();

            } else {
                Toast.makeText(AddPharmacyActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                Toast.makeText(AddPharmacyActivity.this, "Please try again later", Toast.LENGTH_SHORT).show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onVolleyError(String url, String tag, VolleyError error) {

    }
}
