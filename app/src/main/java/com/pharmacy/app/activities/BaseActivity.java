package com.pharmacy.app.activities;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import static com.pharmacy.app.BaseApplication.setCurrentActivity;

public abstract class BaseActivity extends AppCompatActivity {

    public String TAG = "";
    public Resources res = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        res = getResources();
        TAG = getClass().getName();
        setContentView(getLayoutResourceId());
        initViews();
        manipulateViews();
    }

    @Override
    protected void onPause() {
        super.onPause();
        setCurrentActivity(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        TAG = getClass().getName();
        setCurrentActivity(this);
    }

    protected abstract int getLayoutResourceId();

    protected abstract void initViews();

    protected abstract void manipulateViews();
}
