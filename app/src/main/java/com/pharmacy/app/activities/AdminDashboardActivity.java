package com.pharmacy.app.activities;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.pharmacy.app.R;
import com.pharmacy.app.libs.Utils;

public class AdminDashboardActivity extends BaseActivity {

    private Button btn_logout;
    private Button btn_view_pharmacy, btn_add_pharmacy, btn_add_branch_medicine, btn_add_medicine;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_admin_dashboard;
    }

    @Override
    protected void initViews() {

        btn_logout = findViewById(R.id.btn_logout);
        btn_view_pharmacy = findViewById(R.id.btn_view_pharmacy);
        btn_add_pharmacy = findViewById(R.id.btn_add_pharmacy);
        btn_add_branch_medicine = findViewById(R.id.btn_add_branch_medicine);
        btn_add_medicine = findViewById(R.id.btn_add_medicine);
    }

    @Override
    protected void manipulateViews() {

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.clearAllLoginSharePref();
                Intent mIntent = new Intent(AdminDashboardActivity.this, LoginMenuActivity.class);
                startActivity(mIntent);
                Toast.makeText(AdminDashboardActivity.this, "Logout Success!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        btn_view_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(AdminDashboardActivity.this, ViewPharmaciesActivity.class);
                mIntent.putExtra("isAdmin", true);
                startActivity(mIntent);
            }
        });

        btn_add_pharmacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(AdminDashboardActivity.this, AddPharmacyActivity.class);
                mIntent.putExtra("isUpdatePharmacy", false);
                startActivity(mIntent);
            }
        });

        btn_add_branch_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent = new Intent(AdminDashboardActivity.this, AddPharmacyBranchActivity.class);
                startActivity(mIntent);

            }
        });

        btn_add_medicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(AdminDashboardActivity.this, AddMedicineActivity.class);
                startActivity(mIntent);
            }
        });
    }
}
