package com.pharmacy.app.models;

import java.io.Serializable;

public class Medicine implements Serializable {

    public String id;
    public String title;
    public String desc;
    public String price;
    public String mdate;
    public String edate;
    public String img;
    public String bid;
    public String status;
    public String sts;
    public String branch_title;
    public String branch_des;

}