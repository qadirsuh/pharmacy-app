package com.pharmacy.app.models;

import com.pharmacy.app.libs.Utils;

import java.io.Serializable;
import java.util.Date;

public class UserLogin implements Serializable {

    public String id = "";
    public String name = "";
    public String email = "";
    public String phone = "";
    public String status = "";
    public boolean isAdminLogin = false;
    public String loginDate = "";

    public UserLogin() {

    }

    public UserLogin(String id, String name, String email, String phone, String status, boolean isAdminLogin, String loginDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.status = status;
        this.isAdminLogin = isAdminLogin;
        this.loginDate = loginDate;
    }
}
