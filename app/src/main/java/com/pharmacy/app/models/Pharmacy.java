package com.pharmacy.app.models;

import java.io.Serializable;

public class Pharmacy implements Serializable {

    public String id;
    public String title;
    public String desc;
    public String phone;
    public String email;
    public String status;
    public String sts;

}