package com.pharmacy.app;

import android.app.Activity;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.multidex.MultiDex;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.pharmacy.app.activities.LoginMenuActivity;
import com.pharmacy.app.libs.Utils;
import com.pharmacy.app.models.UserLogin;

import java.util.Date;

import io.fabric.sdk.android.Fabric;

public class BaseApplication extends Application {

    public static final String TAG = "VolleyPatterns";
    private static BaseApplication sInstance;
    private static Context context;
    private static Activity activity;
    private RequestQueue mRequestQueue;

    public synchronized static Context getAppContext() {
        return BaseApplication.context;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
    }

    public static Activity getCurrentActivity() {
        return activity;
    }

    public static synchronized BaseApplication getInstance() {
        return sInstance;
    }

    private DatabaseReference mDatabase;

    private static boolean isFirstNotification = true;
    public static boolean isRegisteredNotificationLister = false;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(BaseApplication.this);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        BaseApplication.context = getApplicationContext();

        FirebaseApp.initializeApp(this);
        Fabric.with(this);

        if (Utils.getSharedPrefRefboolean("isAdminLogin")) {
            registerNotificationForAdmin();
        } else {
            Log.e("No body log", "in");
        }

    }

    public static void registerNotificationForAdmin() {

        isRegisteredNotificationLister = true;

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        Query uidRef = rootRef.child("userLoginNotification").limitToLast(1);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Log.d("onDataChange", "called");
                if (!isFirstNotification) {

                    try {
                        Log.d("onDataChange", dataSnapshot.getValue().toString());

                        if (Utils.getSharedPrefRefboolean("isAdminLogin")) {

                            for (DataSnapshot activitySnapShot : dataSnapshot.getChildren()) {
                                UserLogin mUserLogin = activitySnapShot.getValue(UserLogin.class);
                                showNotification(getAppContext(), "New UserLogin login", "UserLogin Name: " + mUserLogin.name);
                                break;
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    isFirstNotification = false;
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage()); //Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public static void showNotification(Context context, String title, String messageBody) {

//        boolean isLoggedIn = SessionManager.getInstance().isLoggedIn();
//        Log.e(TAG, "UserLogin logged in state: " + isLoggedIn);

        Intent intent = new Intent(context, LoginMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        //Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        //Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_app_notification_icon);

        String channel_id = createNotificationChannel(context);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channel_id)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                /*.setLargeIcon(largeIcon)*/
                .setSmallIcon(R.mipmap.ic_launcher) //needs white icon with transparent BG (For all platforms)
                .setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
                .setVibrate(new long[]{1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setAutoCancel(true);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) ((new Date(System.currentTimeMillis()).getTime() / 1000L) % Integer.MAX_VALUE) /* ID of notification */, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String createNotificationChannel(Context context) {

        // NotificationChannels are required for Notifications on O (API 26) and above.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // The id of the channel.
            String channelId = "Channel_id";

            // The user-visible name of the channel.
            CharSequence channelName = "Application_name";
            // The user-visible description of the channel.
            String channelDescription = "Application_name Alert";
            int channelImportance = NotificationManager.IMPORTANCE_DEFAULT;
            boolean channelEnableVibrate = true;
//            int channelLockscreenVisibility = Notification.;

            // Initializes NotificationChannel.
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelName, channelImportance);
            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableVibration(channelEnableVibrate);
//            notificationChannel.setLockscreenVisibility(channelLockscreenVisibility);

            // Adds NotificationChannel to system. Attempting to create an existing notification
            // channel with its original values performs no operation, so it's safe to perform the
            // below sequence.
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(notificationChannel);

            return channelId;
        } else {
            // Returns null for pre-O (26) devices.
            return null;
        }
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        VolleyLog.e("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);

        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}